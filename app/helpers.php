<?php

/**
 * NULL, 空文字, 空配列かをチェックします.
 */
if (!function_exists('is_blank'))
{
    function is_blank($value)
    {
        return is_null($value) || $value === '' || (is_array($value) && !$value);
    }
}

/**
 * エスケープ処理を行います.
 */
if (!function_exists('like_escape'))
{
    function like_escape($str)
    {
        $matches = ['\\\\', '%', '_'];

        if (!is_blank($str))
        {
            foreach ($matches as $match)
            {
                $str = preg_replace("/{$match}/", "\\{$match}", $str);
            }
        }

        return $str;
    }
}
