<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent;
use Illuminate\Database\Query\Builder;

/**
 * UserRepository.
 *
 * @package App\Repositories
 */
class UserRepository extends Repository implements Contracts\UserRepository
{
    /** @var string $default_sort_query デフォルトソート. */
    protected $default_sort_query = 'users.id';

    /**
     * @param $condition
     * @param null $query
     * @return Eloquent\Builder
     */
    protected function applyCondition($condition, $query = null)
    {
        $query = parent::applyCondition($condition, $query);

        foreach ($condition as $key => $value)
        {
            switch ($key)
            {
                case 'code':
                    if (!is_blank($value))
                    {
                        $value = like_escape($value);
                        $query->where('code', 'LIKE', "%$value%");
                    }
                    break;
                case 'name':
                    if (!is_blank($value))
                    {
                        $query->where(function (Builder $query) use ($value) {
                            $value = like_escape($value);
                            $query->where('first_name', 'LIKE', "%$value%");
                        });
                    }
                    break;
            }
        }
        return $query;
    }

}
